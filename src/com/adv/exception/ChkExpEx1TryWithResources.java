package com.adv.exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ChkExpEx1TryWithResources {

	public static void main(String[] args) {
		File file = new File("D://Training//training09//Sample.txt");
		try (FileOutputStream fos = new FileOutputStream(file);) {
			System.out.println("File Operation.....started.....");
			String str = "Writing into the file. Hi, Hello world";
			byte bt[] = str.getBytes();
			fos.write(bt);
			fos.flush();
			System.out.println("File Operation.....ended.....");
		} catch (FileNotFoundException fnfEx) {
			fnfEx.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
