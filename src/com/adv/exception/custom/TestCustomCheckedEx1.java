package com.adv.exception.custom;

public class TestCustomCheckedEx1 {

	public void doOperation() throws CustomCheckedEx1{
		throw new CustomCheckedEx1("FP001", "This has failed due to errorneous input...");
	}
	
	public static void main(String[] args) {
		TestCustomCheckedEx1 testCustomCheckedEx1 = new TestCustomCheckedEx1();
		try {
			testCustomCheckedEx1.doOperation();
		} catch (CustomCheckedEx1 ex) {
			System.out.println("Exception Cause::"+ex.getMessage());
			//e.printStackTrace();
		}
	}
}
