package com.adv.exception;

import java.util.ArrayList;
import java.util.List;

public class UncheckedExp1 {

	public int div1(int a, int b) {
		int result = 0;
		if (b >= 0) {
			result = a/b;
			return result;
		} else {
			throw new RuntimeException("Invalid input. Denominator should be greater than 0");
		}
		//return result;
//		try {
//			return a/b;
//		} catch (Exception ex) {
//			return 0;
//		}
	}
	
	public int div(int a, int b) {
		List<String> ls = null;
//		if (b == 0)
//			throw new RuntimeException("Invalid input");
		//ls = new ArrayList<>();
		if (ls != null) {
			ls.add("Tiger");
		}
		
		return a/b;
	}
	
	public static void main(String[] args) {
		UncheckedExp1 uncheckedExp1 = new UncheckedExp1();
		System.out.println("Div Result="+uncheckedExp1.div(6, 0));
	}
}
