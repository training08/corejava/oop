package com.adv.exception;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CheckedExceptionEx1UsingThrows {

	public static void main(String[] args) throws IOException {
		File file = new File("D://Training//training09//Sample.txt");
		System.out.println("File Operation.....started.....");
		FileOutputStream fos = new FileOutputStream(file);
		String str = "Writing into the file. Hi, Hello world";
		byte bt[] = str.getBytes();
		fos.write(bt);
		fos.flush();
		fos.close();
		System.out.println("File Operation.....ended.....");
	}
}
