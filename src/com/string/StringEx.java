package com.string;

public class StringEx {

	public static void main(String[] args) {
		
		String str = "Lion Tiger Elephnat/Zebra$cat";
		System.out.println(str.trim());
		
		char ch = str.charAt(3);
		System.out.println(ch);

//		String[] stArr = str.split("[/, ,$]");
		String[] stArr = str.split("at");
		for (int i = 0; i < stArr.length; i++) {
			System.out.println(stArr[i]);
		}
		
		//strSamples();
	}

	private static void strSamples() {
		String st = "Tiger";

		//String ref = "value";
		
		StringBuffer stBuff;
		StringBuilder stBuild;
		
		//String ref = new String("value");
		
		String s1 = "Tiger";
		String s2 = "Tiger";
		
		String s3 = new String("Lion");
		String s4 = new String("Lion");
		
		System.out.println("------------Literals----------------");
		if (s1 == s2) {
			System.out.println("value is equal......");
		} else {
			System.out.println("value is not equal......");
		}
		System.out.println("-------------equals()-------------------");
		
		if (s3.equalsIgnoreCase(s4)) {
			System.out.println("value is equal......");
		} else {
			System.out.println("value is not equal......");
		}
		System.out.println("------------Intern()--------------------");
		if (s3.intern() == s4.intern()) {
			System.out.println("value is equal......");
		} else {
			System.out.println("value is not equal......");
		}
		//reverseString("Zebra");
		//reverseStringWithoutInbuilt("Elephant");
		subStrEx("elephant");
	}
	
	public static void reverseString(String input) {
		StringBuilder stBuild = new StringBuilder(input);
		String reverseStr = stBuild.reverse().toString();
		System.out.println(reverseStr);
	}
	
	public static void reverseStringWithoutInbuilt(String input) {
//		int count = input.length();
		char[] ch = input.toCharArray();
		int len = ch.length;
		StringBuilder revStrBuild = new StringBuilder("");
		for (int i = len-1; i >=0; i--) {
			//revStr = revStr + ch[i];
			revStrBuild.append(ch[i]);
		}
		System.out.println(revStrBuild.toString());
	}
	
	public static void subStrEx(String input) {
		String st = input.substring(4, -2);
		System.out.println(st);
	}
}
