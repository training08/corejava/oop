package com.oop.interface1;

public class TestInterfaceEx2 {

	public static void main(String[] args) {
		TestParent testParent = new TestParent();
		//testParent.
		
		TestChild testChild = new TestChild();
		testChild.testM1();
	}
}

class TestParent {
	
}

class TestChild extends TestParent implements Three {
	
	//@Override
	/*public void testM1() {
		System.out.println("Inside TestChild.......... testM1()....");
	}*/
	
	@Override
	public void m1() {
		
	}
	
	@Override
	public void m2() {
		
	}
}

interface One {
	
	public abstract void m1();
}

interface Two /* extends One */ {
	
	public default void testM1() {
		System.out.println("Inside Two --- testM1() ---");
	}
	
	public abstract void m2();
}


interface Three extends One, Two {
	
//	public default void testM1() {
//		System.out.println("Inside Three --- testM1() ---");
//	}
	
	public abstract void m2();
}