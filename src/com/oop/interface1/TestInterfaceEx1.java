package com.oop.interface1;

public class TestInterfaceEx1 {

	public static void main(String[] args) {
		A a = new Child();
		a.m1();
		B b = new Child();
		b.m1();
		System.out.println(A.NAME);
		System.out.println(B.NAME);
		A.m2();
		B b1 = new Child();
		b1.m3();
	}
}

interface A {
	
	String NAME = "Lion";
	
	public void m1();
	
	public static void m2() {
		System.out.println("Inside A-m1()");
	}
}

interface B {
	
	String NAME = "Lion";
	
	public void m1();
	
	public default void m3() {
		System.out.println("Inside default B-m3");
	}
}

class Child implements A, B {
	
	@Override
	public void m1() {
		System.out.println("Inside Child - m1()");
		A.m2();
	}
	
	@Override
	public void m3() {
		System.out.println("Inside default Chilld-m3");
	}
	
	public void test() {
		System.out.println(A.NAME);
	}
}