package com.oop.abstraction;

public class AbstractClassEx3 {

	public static void main(String[] args) {

	}
}

abstract class AbsTest1 {
	
	public int a = 10;
	
	public String name = "XYZ";

	//public abstract void m2();
	
	public void m1() {
		System.out.println("Inside AbsTest1-m1()");
	}
}

class Test2 extends AbsTest1 {
	
	private int val;
	
	public Test2() {
		System.out.println(a);
		val = a;
	}
}
