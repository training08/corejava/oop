package com.oop.abstraction;

public class AbstractClassEx2 {

	public static void main(String[] args) {
//		Parent1 p = new ChildAbs2("Tiger");
		
//		Parent1 p = new Parent1();
		
		Parent1 p = new ChildAbs2();
		System.out.println("name----->"+p.name);
//		p.m1();
//		p.m2();
//		Parent1 p2 = new ChildAbs2("Lion");
		Parent1 p2 = new ChildAbs2();
		System.out.println("name----->"+p2.name);
//		Parent1 p3 = new ChildAbs2("Zebra");
		Parent1 p3 = new ChildAbs2();
		System.out.println("name----->"+p3.name);
	}
}

abstract class Parent1 {
	
	public String name;// = "Elephant";
	
	public Parent1() {
		this.name = "Elephant";
	}
	
	public Parent1(String name) {
		this.name = name;
	}
	
	public abstract void m1();
	
	public abstract void m3();
	
	public void m2() {
		System.out.println("inside m2--");
	}
}

abstract class ChildAbs1 extends Parent1 {
	
	public ChildAbs1() {
	}
	
	public ChildAbs1(String name) {
		super(name);
	}
	
	@Override
	public void m1() {
		System.out.println("inside childabs1 ----m1()----");
	}
}

class ChildAbs2 extends ChildAbs1 {
	
	public ChildAbs2() {
	}
	
	public ChildAbs2(String name) {
		super(name);
	}
	
	@Override
	public void m3() {
		System.out.println("inside childabs1 ----m3()----");
	}
}