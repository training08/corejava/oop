package com.oop.abstraction;

public class AbstractionEx1 {

	public static void main(String[] args) {
		TestAbs testAbs = new TestAbs();
		Parent p = new Parent();
		Child1 c1 = new Child1();
		//Child2 c2 = new Child2();
		Child3 c3 = new Child3();
		
		testAbs.testM1(p);
		testAbs.testM1(c1);
		//testAbs.testM1(c2);
		testAbs.testM1(c3);
	}
}

class Parent {
	
	public void m1() {
		System.out.println("inside parent - m1()");
	}
}

class Child1 extends Parent {
	
	@Override
	public void m1() {
		System.out.println("inside child1 - m1()");
	}
}

abstract class Child2 extends Parent {
	
	public abstract void m3();
	
	@Override
	public void m1() {
		System.out.println("inside child2 - m1()");
	}
}

class Child3 extends Parent {
	
	@Override
	public void m1() {
		System.out.println("inside child3 - m1()");
	}
}

class TestAbs {
	
	public void testM1(Parent p) {
		p.m1();
	}
}