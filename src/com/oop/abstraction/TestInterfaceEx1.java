package com.oop.abstraction;

interface InterfaceEx1 {

	public static final String NAME = "";
	
	public abstract void m1();
}

class ChildImpl1 implements InterfaceEx1 {

	@Override
	public void m1() {
		System.out.println("Inside the ChildImpl1 - m1()");
	}
}

public class TestInterfaceEx1 {
	
	public static void main(String[] args) {
		System.out.println(InterfaceEx1.NAME);
		InterfaceEx1 interfaceEx1 = new ChildImpl1();
		interfaceEx1.m1();
	}
}