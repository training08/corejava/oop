package com.oop.shadowing;

public class ShadowEx1 {

	public static void main(String[] args) {
		Child1 child1 = new Child1();
		child1.m1();
	}
}

class Parent1 {
	
	String name = "Tiger";
}

class Child1 extends Parent1 {
	
	String name = "Lion";
	
	public void m1() {
		String name = "Zebra";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	}
}