package com.oop.serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.oop.pattern.factory.Singleton;

public class DeSerializableEx {

	//public static void main(String[] args) throws IOException, ClassNotFoundException {
	public void deSerialize() throws IOException, ClassNotFoundException {
		String filePath = "D:\\Training\\training08\\corejava\\oop\\src\\com\\oop\\serialization\\single.txt";
		FileInputStream file = new FileInputStream(filePath);
		ObjectInputStream in = new ObjectInputStream(file);

		// Method for deserialization of object
		//Emp emp = (Emp)in.readObject();
		Singleton singleton = (Singleton)in.readObject();
		System.out.println("After DeSerialization......."+singleton);
		in.close();
		file.close();
		System.out.println("DeSerialization is successfull.......");
	}
	
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		DeSerializableEx deSerializableEx = new DeSerializableEx();
		deSerializableEx.deSerialize();
	}
}