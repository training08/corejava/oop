package com.oop.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.oop.pattern.factory.Singleton;

public class SerializableEx {

	//public static void main(String[] args) throws IOException {
	public void serialize() /* throws IOException */ {
		//Emp emp1 = new Emp("Tiger", 10, "t1@gmail.com");
		Singleton singleton = Singleton.getInstance();
		System.out.println("before serialization....." + singleton);
		FileOutputStream file = null;
		ObjectOutputStream out = null;
		try {
			file = new FileOutputStream("D:\\\\Training\\\\training08\\\\corejava\\\\oop\\\\src\\\\com\\\\oop\\\\serialization\\\\single.txt");
			out = new ObjectOutputStream(file);
			out.writeObject(singleton);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				out.close();
				file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("Serialization is successfull.......");
	}
	
	public void serializeTryWithResources() {
		Singleton singleton = Singleton.getInstance();
		System.out.println("before serialization....." + singleton);
		try (FileOutputStream file = new FileOutputStream("D:\\\\Training\\\\training08\\\\corejava\\\\oop\\\\src\\\\com\\\\oop\\\\serialization\\\\single.txt");
				ObjectOutputStream out = new ObjectOutputStream(file);) {
			out.writeObject(singleton);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.out.println("Serialization is successfull.......");
	}
	
	public static void main(String[] args) throws IOException {
		SerializableEx serializableEx = new SerializableEx();
		serializableEx.serialize();
	}
}

class Emp implements Serializable {

	private String name;

	private int age;

	private String email;

	public Emp(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	@Override
//	public String toString() {
//		return "Emp [name=" + name + ", age=" + age + ", email=" + email + "]";
//	}
}