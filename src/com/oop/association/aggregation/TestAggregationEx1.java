package com.oop.association.aggregation;

class School {

	private Student student;
	
	public School(Student student) {
		System.out.println("student obj---"+student);
		this.student = student;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
}

class Student {
	
}

public class TestAggregationEx1 {

	public static void main(String[] args) {
		Student student = new Student();
		School school = new School(student);
		Student student2 = new Student();
		school.setStudent(student2);
		School school1 = new School(student);
	}
}
