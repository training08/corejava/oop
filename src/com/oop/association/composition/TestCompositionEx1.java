package com.oop.association.composition;

class Car {
	
	private Engine engine;
	
	public Car() {
		this.engine = new Engine();
		System.out.println("engine obj-----"+engine);
	}
	
	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public void m1() {
		engine.m1();
	}
}

class Engine {
	
	public void m1() {
		System.out.println("Inside the Engine--m1()");
	}
}

public class TestCompositionEx1 {

	public static void main(String[] args) {
		Car car = new Car();
		//car.m1();
		Car car1 = new Car();
		Engine engine = car1.getEngine();
	}
}
