package com.oop.test_access_specifier;

import com.oop.access_specifier.AccessSpecifierEx1;

public class TestAccessSpecifier {

	public static void main(String[] args) {
		AccessSpecifierEx1 accessSpecifier = new AccessSpecifierEx1();
		//int val = accessSpecifier.b;
		//int val2 = accessSpecifier.e;
		System.out.println("The value of a ="+accessSpecifier.a);
		//System.out.println("The value of b ="+accessSpecifier.b);
		
		ProtectedChild protectedChild = new ProtectedChild();
		System.out.println("The value of c ="+protectedChild.d);
		
		//System.out.println("The value of a ="+accessSpecifier.e);
	}
}
