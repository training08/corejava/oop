package com.oop.test_access_specifier;

import com.oop.access_specifier.AccessSpecifierEx1;

public class ProtectedChild extends AccessSpecifierEx1 {
	
	public int d = c;
	
	@Override
	public void m1() {
		System.out.println("The value of c ="+ c);
//		AccessSpecifierEx1 accessSpecifierEx1 = new AccessSpecifierEx1();
//		accessSpecifierEx1.c;
	}
}
