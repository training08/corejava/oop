package com.oop.objectmethods;

public class EqualsHashToStr {

	public static void main(String[] args) {
		Teacher teacher1 = new Teacher("tiger", 12, "t1@gmail.com");
		Teacher teacher2 = new Teacher("tiger", 12, "t1@gmail.com");
		System.out.println(teacher1.hashCode());
		System.out.println(teacher1.toString());
		System.out.println("Name:"+teacher1.getName()+", age:"+teacher1.getAge()+", email:"+teacher1.getEmail());
		System.out.println("------------------------------");
		System.out.println(teacher2.hashCode());
		System.out.println(teacher2.toString());
		System.out.println("Name:"+teacher2.getName()+", age:"+teacher2.getAge()+", email:"+teacher2.getEmail());
		
		if (teacher1.equals(teacher2)) {
			System.out.println("equal.....");
		} else {
			System.out.println("Not equal.....");
		}
	}
}

class Teacher {

	private String name;

	private int age;

	private String email;

	public Teacher(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getName().equals(((Teacher)obj).getName())) {
			if (this.getAge() == ((Teacher)obj).getAge()) {
				if (this.getEmail().equals(((Teacher)obj).getEmail())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (this.age+this.getName()+this.getEmail()).hashCode();
	}

	@Override
	public String toString() {
		return "The value is ------- Name:" + name + ", age:" + age + ", email=" + email;
	}

//	@Override
//	public String toString() {
//		return "Teacher [name=" + name + ", age=" + age + ", email=" + email + "]";
//	}
}