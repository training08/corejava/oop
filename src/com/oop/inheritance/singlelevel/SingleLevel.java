package com.oop.inheritance.singlelevel;

public class SingleLevel extends SingleLevelParent {
	
	public SingleLevel(int a) {
		super(a);
		System.out.println("Inside child constructor...");
	}
	
	public void m2() {
		System.out.println("Inside m2...........");
	}
}
