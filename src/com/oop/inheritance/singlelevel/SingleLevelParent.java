package com.oop.inheritance.singlelevel;

public class SingleLevelParent {
	
	public int aValue = 10;

	public SingleLevelParent(int a) {
		//new Object();
		System.out.println("Inside parent constructor...");
	}
	
	public void m1() {
		System.out.println("Inside m1...........");
	}
}
