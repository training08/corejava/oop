package com.oop.inheritance.hierarchial;

public class TestHierarchial {

	public static void main(String[] args) {
		Child1 child1 = new Child1();
		child1.m1();
		child1.m2();
		Child2 child2 = new Child2();
		child2.m1();
		child2.m3();
		Child3 child3 = new Child3();
		child3.m1();
		child3.m4();
	}
}
