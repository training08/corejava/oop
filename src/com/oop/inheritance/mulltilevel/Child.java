package com.oop.inheritance.mulltilevel;

public class Child extends Parent {
	
	public Child(int a) {
		super(a);
		System.out.println("Inside child constructor...");
	}
	
	public void m2() {
		System.out.println("Inside m2...........");
	}
}
