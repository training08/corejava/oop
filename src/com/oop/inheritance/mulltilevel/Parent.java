package com.oop.inheritance.mulltilevel;

public class Parent extends GrandParent {
	
	public int aValue = 10;

	public Parent(int a) {
		super(a);
		System.out.println("Inside parent constructor...");
	}
	
	public void m1() {
		System.out.println("Inside m1...........");
	}
}
