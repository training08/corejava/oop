package com.oop.encapsulation;

class ParentCl {
	public String name1 = "Tiger";
}

public class SuperThis extends ParentCl{

	public String name2 = "Lion";
	
	public void m1() {
		String name3 = "Zebra";
//		System.out.println("local variable-->"+name);
//		System.out.println("Global variable-->"+this.name);
//		System.out.println("Super variable-->"+super.name);
		System.out.println("local variable-->"+name3);
		System.out.println("Global variable-->"+this.name2);
		System.out.println("Super variable-->"+super.name1);
	}
	
	public static void main(String[] args) {
		SuperThis superThis = new SuperThis();
		superThis.m1();
	}
}
