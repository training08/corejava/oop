package com.oop.encapsulation;

class EmpParent {
	
	public String name;
	
	Employee emp = new Employee("Tiger", 11);
	
	public EmpParent() {
		System.out.println("EmpParent - default constructor");
	}
	
	public EmpParent(String name) {
		System.out.println("EmpParent - default constructor");
	}
}

public class Employee extends EmpParent {

	private String name;

	private int age;
	
	private String email;

	/*public Employee() {
//		this.name = "Tiger";
//		this.age = 10;
		System.out.println("Inside default constructor......");
	}*/

	public Employee(String name, int age) {
		super(name);
//		super("tiger");
//		this();
		System.out.println("EmpParent - default constructor");
		this.name = name;
		this.age = age;
		System.out.println("Inside parameterized constructor......");
	}
	
	public Employee(int age, String name) {
		this.name = name;
		this.age = age;
		System.out.println("Inside parameterized constructor......");
	}
	
	public Employee(String name, int age, String email) {
		this.name = name;
		this.age = age;
		this.email = email;
		System.out.println("Inside parameterized constructor......");
	}
	
	public Employee(String name, String email, int age) {
		this.name = name;
		this.age = age;
		this.email = email;
		System.out.println("Inside parameterized constructor......");
	}
	
	public Employee(int age, String name, String email) {
		this.name = name;
		this.age = age;
		this.email = email;
		System.out.println("Inside parameterized constructor......");
	}
	
	public void m1(String name) {
		System.out.println("Name="+name);
	}
}
