package com.oop.blocks;

class TestBlocksParent {
	
	public TestBlocksParent() {
		System.out.println("Inside TestBlocksParent constructor.........");
	}
}

public class TestBlocks extends TestBlocksParent {

	private final String value;
	
	private String name;
	
	static {
		System.out.println("Inside static block.......");
	}
	
	public TestBlocks(String name) {
		//value = "10";
		int ageVal = 10;
		System.out.println(ageVal);
		System.out.println("Inside the Constructor.......");
		//this.name = name;
		//m1();
	}
	
	{
		System.out.println("Inside the blocks.......");
		value = "xyz";
		//name = "";
		//m1();
	}
	
	public void m1() {
		System.out.println("inside m1()......");
	}
	
	public static void main(String[] args) {
		TestBlocks testBlocks = new TestBlocks("Tiger");
		TestBlocks testBlocks1 = new TestBlocks("Lion");
		TestBlocks testBlocks2 = new TestBlocks("Zebra");
	}
}
