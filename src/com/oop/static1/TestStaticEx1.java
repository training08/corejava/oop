package com.oop.static1;

class StaticEx1 {

	public static String NAME = "Lion";
	
	public String email = "";
	
	static {
		NAME = "Cow";
	}
	
	public String age;
	
	public StaticEx1(String name) {
		this.NAME = name;
	}
	
	public static void m1() {
		System.out.println(new StaticEx1("").email);
		System.out.println("Inside - static m1()");
	}
}

public class TestStaticEx1 {
	
	public static void main(String[] args) {
		StaticEx1 staticEx1 = new StaticEx1("Tiger");
		System.out.println(staticEx1.NAME);
		System.out.println("----------------------");
		
		StaticEx1 staticEx2 = new StaticEx1("Zebra");
		System.out.println(staticEx2.NAME);
		System.out.println(staticEx1.NAME);
		System.out.println("----------------------");
		
		StaticEx1 staticEx3 = new StaticEx1("Elephant");
		System.out.println(staticEx3.NAME);
		System.out.println(staticEx2.NAME);
		System.out.println(staticEx1.NAME);
		System.out.println("----------------------");
		
		System.out.println("Class call ------>"+StaticEx1.NAME);
		
		StaticEx1.m1();
		new StaticEx1("").m1();
		StaticEx1 staticEx4 = new StaticEx1("");
		staticEx4.m1();
	}
}