package com.oop.immutable;

import java.util.ArrayList;
import java.util.List;

public class TestImmutable {

	public static void main(String[] args) {
		List<String> strList = new ArrayList<>();
		strList.add("A");
		strList.add("B");

		ImmutableEx immutableEx = new ImmutableEx("Tiger", strList);
		System.out.println("immutableEx---->" + immutableEx);
		// immutableEx.setName("Lion");
		System.out.println("immutableEx---->" + immutableEx.getName());
		
		List<String> strList1 = immutableEx.getList();
		strList1.add("C");
		System.out.println("immutableEx---->" + immutableEx);
		System.out.println(strList1);
		
//		ImmutableChild immutableChild = new ImmutableChild("Tiger");
//		System.out.println("immutableEx---->" + immutableChild);
//		// immutableEx.setName("Lion");
//		System.out.println("immutableEx---->" + immutableChild.getName());
		
		
	}
}

final class ImmutableEx {

	private final String name;
	
	private final List<String> list;

	public ImmutableEx(String name, List<String> list) {
		this.name = name;
		this.list = list;
	}

	public String getName() {
		return name;
	}
	
	public List<String> getList() {
		return new ArrayList<>(list);
	}

//	public void setName(String name) {
//		this.name = name;
//	}

	@Override
	public String toString() {
		return "Name::" + name + ", List value="+getList();
	}
}

//class ImmutableChild extends ImmutableEx {
//
//	public ImmutableChild(String name) {
//		super(name);
//	}
//	
//	@Override
//	public String getName() {
//		return super.getName() + 1;
//	}
//}