package com.oop.pattern.factory;

public class TestFactoryPattern {

	public static void main(String[] args) {
		BiscuitFactory biscuitFactory = new BiscuitFactory();
		Biscuits biscuits = biscuitFactory.getBiscuit("ParleG");
		biscuits.create();
	}
}

class BiscuitFactory {
	
	public Biscuits getBiscuit(String name) {
		if (name.equalsIgnoreCase("GoodDay")) {
			return new GoodDay();
		} else if (name.equalsIgnoreCase("ParleG")) {
			return new ParleG();
		} else if (name.equalsIgnoreCase("MarieGold")) {
			return new MarieGold();
		} else {
			throw new RuntimeException("No such Biscuits");
		}
	}
}

interface Biscuits {
	public void create();
}

class GoodDay implements Biscuits {
	
	@Override
	public void create() {
		System.out.println("GoodDay biscuit created......");
	}
}

class ParleG implements Biscuits {
	
	@Override
	public void create() {
		System.out.println("ParleG biscuit created......");
	}
}

class MarieGold implements Biscuits {
	
	@Override
	public void create() {
		System.out.println("MarieGold biscuit created......");
	}
}