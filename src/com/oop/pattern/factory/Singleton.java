package com.oop.pattern.factory;

import java.io.Serializable;

public class Singleton implements Cloneable, Serializable {

//	//eager initialization
//	private static Singleton singleton = new Singleton();

	// laxy initialization
	private static Singleton singleton;

//	public static Singleton getInstance() {
//		return singleton;
//	}

	// Lazy initialization
	public static /* synchronized */ Singleton getInstance() {
		if (singleton == null) {
			synchronized (Singleton.class) {
				if (singleton == null) {
					singleton = new Singleton();
				}
			}
		}
		return singleton;
	}

	private Singleton() {
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// return super.clone();
		throw new CloneNotSupportedException("Object clone not supported...");
	}

	protected Object readResolve() {
		System.out.println("inside the readResolve........obj value="+singleton);
		return singleton;
	}
}