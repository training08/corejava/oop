package com.oop.pattern.factory;

import java.io.IOException;

import com.oop.serialization.DeSerializableEx;
import com.oop.serialization.SerializableEx;

public class TestSingletonPattern {

	public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException, InterruptedException {
		Singleton singletonObj = Singleton.getInstance();
		
		Singleton singletonObj1 = Singleton.getInstance();
		
		System.out.println("singletonObj="+singletonObj);
		
		System.out.println("singletonObj1="+singletonObj1);
		
		SerializableEx serializableEx = new SerializableEx();
		serializableEx.serialize();
		
		DeSerializableEx deSerializableEx = new DeSerializableEx();
		deSerializableEx.deSerialize();
		
		//Thread.sleep(100000);
		
		Singleton singleton1 = (Singleton)singletonObj.clone();
		System.out.println("singleton1="+singleton1);
		Singleton singleton2 = (Singleton)singletonObj.clone();
		System.out.println("singleton2="+singleton2);
	}
}