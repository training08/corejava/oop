package com.oop.clone;

public class TestCloning {

	public static void main(String[] args) throws CloneNotSupportedException {
		Address address = new Address("KamarajNagar", 456789, "KGI");
		Employee employee = new Employee("Tiger", 10, "t1@gmail.com", address);
		Employee employee1 = (Employee) employee.clone();
		System.out.println("Before change.............");
		System.out.println("employee="+employee);
		System.out.println("employee1="+employee1);
		
		employee1.setName("Lion");
		Address address1 = employee1.getAddress();
		address1.setDistrict("Blr");
		
		System.out.println("After change.............");
		System.out.println("employee="+employee);
		System.out.println("employee1="+employee1);
	}
}
