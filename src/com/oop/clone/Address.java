package com.oop.clone;

public class Address implements Cloneable {

	private String streetName;
	
	private int pincode;
	
	private String district;

	public Address(String streetName, int pincode, String district) {
		super();
		this.streetName = streetName;
		this.pincode = pincode;
		this.district = district;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	@Override
	public String toString() {
		return "Address [streetName=" + streetName + ", pincode=" + pincode + ", district=" + district + "]";
	}
}
