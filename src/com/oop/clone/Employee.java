package com.oop.clone;

public class Employee implements Cloneable {

	private String name;
	
	private int id;
	
	private String email;
	
	private Address address;

	public Employee(String name, int id, String email, Address address) {
		super();
		this.name = name;
		this.id = id;
		this.email = email;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	//shallow cloning
//	@Override
//	protected Object clone() throws CloneNotSupportedException {
//		return super.clone();
//	}
	
	//deep cloning
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Employee employee = (Employee)super.clone();
		Address address = (Address) employee.getAddress().clone();
		employee.setAddress(address);
		return employee;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + ", email=" + email + ", address=" + address + "]";
	}
}
