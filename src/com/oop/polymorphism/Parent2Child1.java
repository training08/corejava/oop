package com.oop.polymorphism;

public class Parent2Child1 {

	public static void main(String[] args) {
		GrandParent grandParent = new Child();
		grandParent.m1();
		
		GrandParent grandParent1 = new Parent();
		grandParent1.m1();
	}
}

class GrandParent {
	public void m1() {
		System.out.println("Inside m1 - GrandParent....");
	}

	public void m2() {
		System.out.println("Inside m2 - GrandParent....");
	}
}

class Parent extends GrandParent {
	
	@Override
	public void m2() {
		System.out.println("Inside m2 - Parent....");
	}
}

class Child extends Parent {
	
	@Override
	public void m1() {
		System.out.println("Inside m1 - Child....");
	}

	public void m3() {
		System.out.println("Inside m3 - Child....");
	}
}
