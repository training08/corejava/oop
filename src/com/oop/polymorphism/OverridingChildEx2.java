package com.oop.polymorphism;

public class OverridingChildEx2 extends OverridingParentEx {
	
	public void m1() {
		System.out.println("inside OverridingChildEx2 - child - m1......");
	}
	
	public void m4() {
		System.out.println("inside OverridingChildEx2 - child - m3......");
	}
}
