package com.oop.polymorphism;

public class TestDynamicOverriding {

	public static void main(String[] args) {
		/*
		 * OverridingEx overridingEx = new OverridingEx(); overridingEx.m1();
		 * overridingEx.m2(); OverridingParentEx overridingParentEx = new
		 * OverridingParentEx(); overridingParentEx.m1();
		 */
//		OverridingParentEx overridingParentEx1 = new OverridingChildEx1();
//		overridingParentEx1.m1();
		
		DynamicPoly dynamicPoly = new DynamicPoly();
		
		OverridingChildEx1 overridingChildEx1 = new OverridingChildEx1();
		dynamicPoly.testDynamicPoly(overridingChildEx1);
		
		OverridingChildEx2 overridingChildEx2 = new OverridingChildEx2();
		dynamicPoly.testDynamicPoly(overridingChildEx2);

		//dynamicPoly.testDynamicPoly(new OverridingChildEx1());
	}
}
