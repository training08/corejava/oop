package com.oop.polymorphism;

public class OverridingParentEx {

	public void m1() {
		System.out.println("inside OverridingParentEx - Parent - m1......");
	}
	
	public void m2() {
		System.out.println("inside OverridingParentEx - Parent - m2......");
	}
}
