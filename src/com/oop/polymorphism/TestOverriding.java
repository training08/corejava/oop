package com.oop.polymorphism;

public class TestOverriding {

//	public static void testM1(OverridingChildEx1 childEx1) {
//		childEx1.m1();
//	}

	public static void testM1(OverridingParentEx parentEx1) {
		parentEx1.m1();
	}
	
	public static void main(String[] args) {
//		OverridingParentEx parent1 = new OverridingChildEx1();
//		parent1.m1();
//		
//		OverridingParentEx parent2 = new OverridingChildEx2();
//		parent2.m1();
		
		OverridingChildEx1 childEx1 = new OverridingChildEx1();
		OverridingChildEx2 childEx2 = new OverridingChildEx2();
		OverridingChildEx3 childEx3 = new OverridingChildEx3();
		
		testM1(childEx1);
		testM1(childEx2);
		testM1(childEx3);
	}
}
