package com.oop.polymorphism;

public class OverridingChildEx3 extends OverridingParentEx {
	
	public void m1() {
		System.out.println("inside OverridingChildEx3 - child - m1......");
	}
	
	public void m4() {
		System.out.println("inside OverridingChildEx3 - child - m3......");
	}
}
