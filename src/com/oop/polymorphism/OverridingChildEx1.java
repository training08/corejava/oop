package com.oop.polymorphism;

public class OverridingChildEx1 extends OverridingParentEx {
	
	public void m1() {
		System.out.println("inside OverridingChildEx1 - child - m1......");
	}
	
	public void m3() {
		System.out.println("inside OverridingChildEx1 - child - m3......");
	}
}
