package com.oop.polymorphism;

public class DynamicPoly {

//	public void testDynamicPoly(OverridingChildEx1 overridingChildEx1) {
//		overridingChildEx1.m1();
//	}
//	
//	public void testDynamicPoly(OverridingChildEx2 overridingChildEx2) {
//		overridingChildEx2.m1();
//	}
	
//	public void testDynamicPoly(OverridingChildEx3 overridingChildEx3) {
//		overridingChildEx3.m1();
//	}
	
	public void testDynamicPoly(OverridingParentEx overridingParentEx) {
		overridingParentEx.m1();
	}
}
