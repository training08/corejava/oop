package com.thread;

class ThreadEx1 implements Runnable {

	@Override
	public void run() {
		System.out.println("Inside the run method - Implements Runnable");
	}
}

class ThreadEx2 extends Thread {

	@Override
	public void run() {
		System.out.println("Inside the run method - Extends Thread");
	}
}

public class TestThreadEx1 {
	
	public static void main(String[] args) {
		System.out.println("Inside main......start.....");
		ThreadEx1 threadEx1 = new ThreadEx1();
		Thread t1 = new Thread(threadEx1);
		t1.start();
		
		ThreadEx2 t2 = new ThreadEx2();
		t2.start();
		System.out.println("Inside main......end.....");
		
		
	}
}