package com.stringtokenizer;

import java.util.StringTokenizer;

public class StringTokenizerEx {

	public static void main(String[] args) {
		String str = "Lion/Tiger/Elephnat Zebra$cat,rat[monkey]donkey";
		StringTokenizer stTokens = new StringTokenizer(str, "[/, $]"); 
		while (stTokens.hasMoreElements()) {
			String token = (String) stTokens.nextElement();
			System.out.println("Token="+token);
		}
	}
}
